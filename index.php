<?php

class Falabella {
    
    var $inicio = 1;
    var $final = 100;
    
    function realiza_accion() {
        $arrayData = array('15'=>'Integraciones', '5'=>'IT', '3'=>'Falabella');
        for ($i=$this->inicio; $i<=$this->final; $i++) {
            echo $this->revisa_numero($i, $arrayData);
        }
        return true;
    }

    function revisa_numero($numero, $arrayData) {
        foreach ($arrayData AS $key => $value) {
            if ($numero%$key == 0) {
                return $value."<br>";
            }
        }
        return $numero."<br>";
    }

    function getInicio() {
        return $this->inicio;
    }

    function getFin() {
        return $this->final;
    }

    function setInicio($inicio) {
        $this->inicio =  $inicio;
    }

    function setFin($final) {
        $this->final = $final;
    }
}

$falabella = new Falabella();
$falabella->setInicio(1);
$falabella->setFin(100);
echo $falabella->realiza_accion();